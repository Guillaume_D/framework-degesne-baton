#Manuel d'utilisation

- Les **titres** sont en gras et en noir, les sous-titres eux sont en italique et en gris. La police d'écriture est `Segoe Print` qui change un peu de l'ordinaire.

- Tout le reste du texte est aussi en `Segoe Print` avec la taille de base.

- Utiliser les classes `.right`, `.left`, `.center` pour l'alignement du texte, ainsi que .maj et .min pour les transformations de texte.

- Pour les **boutons**, il faut utiliser la classe `.btn`, suivi des classes `.btn-big` ou `.btn-small`, si l'on veut un gros ou petit boutons. On peut aussi utiliser les classes, `.btn-red`, `.btn-blue` et `.btn-green` pour des boutons en couleur. Les boutons en couleur possèdent une fonction en "hover" où leur couleur s'éclaircie.

- La **barre de navigation** est modifier avec la classe `.menu`, la couleur du background est grise et le texte est noir. Elle place les attribut de la liste (li) l'un à côté de l'autre et enlève les puces. Cette classe aussi possède une fonction en hover qui encadre l'attribut dans la liste qui est sélectionné.

- Les **messages d'alertes** utilisent la classe `.alert` pour la forme et quatre autres classes en fonction du type de l'alerte avec chacun une couleur différente : `.alert-info (bleu), .alert-warning (jaune orangé), .alert-success (vert), .alert-error (rouge).`

- La **grille** actuelle peut aller jusqu'à `8è` colonnes.